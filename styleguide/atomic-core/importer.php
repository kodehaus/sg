<?php
include("head.php");
?>
<body class="atoms" xmlns="http://www.w3.org/1999/html">

<div class="pageHeader">
    <img class="logo" src="http://atomicdocs.io/img/atomic-logo.svg">
</div>
<div class="pageContent">
    <ol>
        <li>In this copy of Atomic Docs remove the components and scss folders.</li>
        <li>Copy the components and scss folders you would like to import into this copy of Atomic Docs.</li>
        <!--<li>Fill in the form below and submit. If you are importing from an old version of Atomic Docs keep the defaults.</li>-->
        <li>Click import.</li>
        <li>Compile your scss.</li>
        <li>Unfortunately the importer will not include any of your component descriptions or contextual background
            colors so you will need to add those manually.
        </li>
    </ol>
    <form id="importForm">
        <button class="aa_btn aa_btn-pos" type="submit">Import</button>
    </form>
</div>
<?php include("footer.php"); ?>
</body>





