<?php

function getConfig(){
  $config_json = file_get_contents('../../../styleguide.json');
  if(!$config_json) {
    echo 'Please add a styleguide.json file at your project root.';
    return false;
  }
  return json_decode($config_json, true);
}

$config = getConfig();
buildJSAndCSS();

//BuildOneHtmlWithAllTemplates();
function BuildOneHtmlWithAllTemplates(){
  $components = array(
    'atoms'    ,
    'organisms' ,
    'molecules'
  );

  $html = scandir('../../components/atoms');

  $totalHtml= "<!DOCTYPE html><meta charset=utf-8><title>LESS boilerplate</title><meta content=\"IE=edge,chrome=1\"http-equiv=X-UA-Compatible><meta content=\"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0\"name=viewport><meta content=yes name=apple-mobile-web-app-capable><meta content=black-translucent name=apple-mobile-web-app-status-bar-style><link href=dist/css/vendors.css rel=stylesheet><link href=dist/css/main.css rel=stylesheet><style>.container{padding:20px 0}.bg-1{background-color:#084466}.bg-2{background-color:#cfd1d3}.bg-2{background-color:#f6f6f7}</style><div class='container'>";

  foreach ($components as $folder) {
    $folder_items = scandir('../../components/'.$folder);
    foreach ($folder_items as $file) {
      if (strpos($file, 'html') !== false ) {

        $totalHtml .= file_get_contents('../../components/'.$folder.'/'.$file);
      }
    }
  }

  $totalHtml.= "</div></body>";
  /*
  $totalHtml.= "<script>window.jQuery || document.write('<script src=\"ist/js/jquery.js><\/script>')</script>";
  $totalHtml.= '<script src="dist/js/vendors.js"></script>';
  $totalHtml.= '<script src="dist/js/scripts.js"></script>';*/
  $totalHtml .= '<script>window.jQuery || document.write("<script src=dist/js/jquery.js><\/script>")</script><script src=dist/js/vendors.js></script><script src=dist/js/scripts.js></script>';

    if (!file_exists('../../standalone/' . $category_name)) {
      mkdir('../../standalone/' . $category_name, 0777, TRUE);
    }
  file_put_contents('../../standalone/index.html', $totalHtml);

}
function buildJSAndCSS() {
  error_reporting(-1);
  buildMainLessFile();
  exec('gulp build', $output, $returnCode);
  $output = implode(PHP_EOL, $output);
  if($returnCode === 0) {
    header("location:../index.php?build=success");
  } else {
    echo $output . ' || error :  ' . $returnCode;
  }
}

function buildMainLessFile() {
  $file_main_less = file_get_contents('../../../app/less/import/import.less');
  $file_main_less .= PHP_EOL;
  $atoms_array = scandir('../../../app/less/atoms');
  $organisms_array = scandir('../../../app/less/organisms');
  $molecules_array = scandir('../../../app/less/molecules');
  $sample_pages_array = scandir('../../../app/less/sample_pages');
  $less_files = array(
    'atoms'     => $atoms_array,
    'organisms' => $organisms_array,
    'molecules' => $molecules_array,
    'sample_pages' => $sample_pages_array
  );

  add_category_if_not_exist_in_json_file('categories', 'category', 'atoms');
  add_category_if_not_exist_in_json_file('categories', 'category', 'organisms');
  add_category_if_not_exist_in_json_file('categories', 'category', 'molecules');
  add_category_if_not_exist_in_json_file('categories', 'category', 'sample_pages');

  foreach ($less_files as $folder => $folder_items) {
    foreach ($folder_items as $file) {
      if (strpos($file, 'less') !== false && strpos($file, 'atoms') === false && strpos($file, 'molecules') === false && strpos($file, 'organisms') === false && strpos($file, 'sample_pages') === false) {
        add_component_if_not_exist_in_json_file('components','component',$file,$folder);
        $file_main_less .= '@import "'.$folder.'/'.$file.'";'.PHP_EOL;
      }
      }
  }
  file_put_contents('../../../app/less/main.less', $file_main_less);
}


function add_component_if_not_exist_in_json_file($file_name, $item_name, $value, $category_name) {

  if ($value[0] === '_') {
    $value = substr($value, 1);
  }
  $value = str_replace('.less', '', $value);

  $file_content = json_decode(file_get_contents('../../atomic-db/' . $file_name . '.dat'), TRUE);
  $js_exist = file_exists('../../js/' . $value);
  $php_file_exist = file_exists('../../components/' . $category_name . '/' . $value . '.html');
  if (!$php_file_exist) {
    if (!file_exists('../../components/' . $category_name)) {
      mkdir('../../components/' . $category_name, 0777, TRUE);
    }

    file_put_contents('../../components/' . $category_name . '/' . $value . '.html', '');
  }


  if (!find_key_value_in_array($file_content, $item_name, $value)) {
    array_push($file_content, [$item_name      => $value,
                               "order"         => 0,
                               "category"      => $category_name,
                               'has_js'        => $js_exist,
                               'description'   => '',
                               'backgroundColor' => ''
    ]);
  }
  $file_content = json_encode($file_content);


  file_put_contents('../../atomic-db/' . $file_name . '.dat', $file_content);
}

function add_category_if_not_exist_in_json_file($file_name, $item_name, $value) {

  $file_content = json_decode(file_get_contents('../../atomic-db/' . $file_name . '.dat'), TRUE);


  if (!find_key_value_in_array($file_content, $item_name, $value)) {
    array_push($file_content, [$item_name => $value, "order" => 0]);
  }
  $file_content = json_encode($file_content);


  file_put_contents('../../atomic-db/' . $file_name . '.dat', $file_content);
}

function find_key_value_in_array($items, $key, $value) {
  foreach ($items as $item) {
    if ($item[$key] == $value) {
      return TRUE;
    }

  }
}
