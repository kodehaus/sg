<!-- Place any head info you would like shared between the styleguide and the root of your project. Eg. Links to js scripts etc.. -->

<?php
$config_json = file_get_contents('../../styleguide.json');
if(!$config_json) {
  die('Please add a styleguide.json file at your project root.');
}
$config_array = json_decode($config_json, true);
?>
<link rel="stylesheet" href="../../<?php echo $config_array['css']['dest'] ?>/vendors.css">
<link rel="stylesheet" href="../../<?php echo $config_array['css']['dest'] ?>/main.css">

