'use strict';

var config = require('./styleguide.json');
var runSequence = require('run-sequence');

// Load Gulp
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    plugins = require('gulp-load-plugins')({
        rename: {
            'run-sequence': 'runSequence'
        }
    });

// Start Watching: Run "gulp"
gulp.task('default', ['watch']);

// Copy fonts
gulp.task('fonts', function() {
    gulp.src(config.fonts.src + '/**/*.{ttf,woff,woff2,eof,svg}')
        .pipe(gulp.dest(config.fonts.dest));
});

// Minify Custom JS: Run manually with: "gulp build-js"
gulp.task('build-js', function () {
    return gulp.src(config.js.src)
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'))
        .pipe(plugins.uglify({
            output: {
                'ascii_only': true
            }
        }))
        .pipe(plugins.concat('scripts.js'))
        .pipe(gulp.dest(config.js.dest));
});

gulp.task('build-js-vendors', function () {
    return gulp.src(config.vendors.js)
        .pipe(plugins.uglify({
            output: {
                'ascii_only': true
            }
        }))
        .pipe(plugins.concat('vendors.js'))
        .pipe(gulp.dest(config.js.dest));
});


// Less to CSS: Run manually with: "gulp build-css"
gulp.task('build-css', function () {
    return gulp.src(config.css.src)
        .pipe(plugins.plumber())
        .pipe(plugins.less())
        .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        })
        .pipe(plugins.autoprefixer({
            browsers: [
                    '> 1%',
                    'last 2 versions',
                    'firefox >= 4',
                    'safari 7',
                    'safari 8',
                    'IE 8',
                    'IE 9',
                    'IE 10',
                    'IE 11'
                ],
            cascade: false
        }))
        .pipe(plugins.cssmin())
        .pipe(gulp.dest(config.css.dest)).on('error', gutil.log);

});


gulp.task('build-css-vendors', function () {
    return gulp.src(config.vendors.css)
        .pipe(plugins.plumber())
        .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        })
        .pipe(plugins.cssmin())
        .pipe(plugins.concat('vendors.css'))
        .pipe(gulp.dest(config.css.dest)).on('error', gutil.log);
});

// Default task
gulp.task('watch', function () {
    gulp.watch(config.js.src, ['build-js']);
    gulp.watch(config.css.src, ['build-css']);
});

/**
 * Cleans Build directory
 */
gulp.task('clean', function () {
    return gulp.src(config.build, {read: false})
        .pipe(plugins.clean());
});

/**
 * Gulp build
 */
gulp.task('build', function (callback) {
    runSequence('clean',
        ['build-js', 'build-js-vendors','build-css','build-css-vendors', 'fonts'],
        callback);
});